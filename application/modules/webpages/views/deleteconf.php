<h1 style="margin-bottom: 20px;"><?= $headline ?></h1>

<?= validation_errors("<p style='color: red';>", '</p>') ?>

<?php 
	if(isset($flash)){
		echo $flash;
	}
?>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Hapus Data</h2>
			<div class="box-icon">
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		
		<div class="box-content text-center" style="padding: 10px;">
			<h4>Apakah kamu yakin ingin menghapus halaman ini ?</h4>
			<?php
				$attributes = array('class' => 'form-horizontal'); 
				echo form_open('webpages/delete/'.$update_id, $attributes);
			?>
			  <fieldset>
				<div class="control-group" style="height: 200px;">
				   <button type="submit" name="submit" value="Hapus" class="btn btn-danger">Hapus</button>
				   <button type="submit" name="submit" value="Cancel" class="btn">Batal</button>
				</div>    

			  </fieldset>
			</form>  
		</div>  
	</div><!--/span-->
</div><!--/row-->