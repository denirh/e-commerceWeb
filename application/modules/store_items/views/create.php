<h1 style="margin-bottom: 20px;"><?= $headline ?></h1>

<?= validation_errors("<p style='color: red';>", '</p>') ?>

<?php 
	if(isset($flash)){
		echo $flash;
	}
?>

<?php if(is_numeric($update_id)) { ?>
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Item Options</h2>
			<div class="box-icon">
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		
		<div class="box-content" style="padding: 10px;">
		<?php if($big_pic==""){ ?>
			<a href="<?= base_url(); ?>store_items/upload_image/<?= $update_id ?>"><button type="button" class="btn btn-primary">Upload Item Image</button></a>
		<?php }else{ ?>
			<a href="<?= base_url(); ?>store_items/delete_image/<?= $update_id ?>"><button type="button" class="btn btn-danger">Delete Item Image</button></a>
		<?php } ?>

			<a href="<?= base_url(); ?>store_item_colours/update/<?= $update_id ?>"><button type="button" class="btn btn-primary">Update Item Colour</button></a>
			<a href="<?= base_url(); ?>store_item_sizes/update/<?= $update_id ?>"><button type="button" class="btn btn-primary">Update Item Sizes</button></a>
			<a href="<?= base_url(); ?>store_cat_assign/update/<?= $update_id ?>"><button type="button" class="btn btn-primary">Update Item Categories</button></a>
			<a href="<?= base_url(); ?>store_items/deleteconf/<?= $update_id ?>"><button type="button" class="btn btn-danger">Delete Item</button></a>
			<a href="<?= base_url(); ?>store_items/view/<?= $update_id ?>"><button type="button" class="btn btn-info">View Item In Shop</button></a>
		</div>  

	</div><!--/span-->
</div><!--/row-->
<?php  } ?>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Item Detail</h2>
			<div class="box-icon">
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<?php 
				$form_location = base_url()."store_items/create/".$update_id;
			?>
			<form class="form-horizontal" method="POST" action="<?= $form_location; ?>">
			  <fieldset>
				<div class="control-group">
				  <label class="control-label" for="typeahead">Item Title </label>
				  <div class="controls">
					<input type="text" class="span6" id="typeahead" name="item_title" value="<?= $item_title; ?>">
				  </div>
				</div>

				<div class="control-group">
				  <label class="control-label" for="typeahead2">Item Price </label>
				  <div class="controls">
					<input type="text" class="span6" id="typeahead2" name="item_price" value="<?= $item_price; ?>">
				  </div>
				</div>

				<div class="control-group">
				  <label class="control-label" for="typeahead3">Was Price <span style="color: green;">(optional)</span> </label>
				  <div class="controls">
					<input type="text" class="span6" id="typeahead3" name="was_price" value="<?= $was_price; ?>">
				  </div>
				</div>

				<div class="control-group">
				  <label class="control-label" for="selectError3">Status </label>
				  <div class="controls">

					<?php 
						$data_id_dd = 'id="selectError3"';
						$options = array(
									''     => 'Please select...',
							        '1'    => 'Active',
							        '0'    => 'Inactive',
							);

						echo form_dropdown('status', $options, $status, $data_id_dd); 
					?>

				  </div>
				</div>
				            
				<div class="control-group hidden-phone">
				  <label class="control-label" for="textarea2">Item Description</label>
				  <div class="controls">
					<textarea class="cleditor" id="textarea2" rows="3" name="item_description"><?php echo $item_description ?></textarea>
				  </div>
				</div>
				<div class="form-actions">
				  <button type="submit" class="btn btn-primary" name="submit" value="Submit">Save changes</button>
				  <button type="submit" class="btn" name="submit" value="Cancel">Cancel</button>
				</div>
			  </fieldset>
			</form>   

		</div>
	</div><!--/span-->

</div><!--/row-->

<?php if($big_pic!=""){ ?>
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Image Detail</h2>
			<div class="box-icon">
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		
		<div class="box-content" style="padding: 10px;">
			<img src="<?= base_url()?>big_pics/<?= $big_pic ?>" style="width: 50%; height: auto;">
		</div>  

	</div><!--/span-->
</div><!--/row-->
<?php } ?>