<div class="row">
	<div class="col-md-4">
		<img src="<?= base_url() ?>big_pics/<?= $big_pic ?>" class="img-responsive" alt="<?= $item_title; ?>" style="margin-top: 24px;">
	</div>
	<div class="col-md-5">
		<h1><?= $item_title ?></h1>
		<div class="description" style="clear: both;">
			<p><?= nl2br($item_description) ?></p>
		</div>
	</div>
	<div class="col-md-3">
		<?= Modules::run('cart/_draw_add_to_cart', $update_id); ?>
	</div>
</div>