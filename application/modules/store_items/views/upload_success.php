<h1 style="margin-bottom: 20px;"><?= $headline ?></h1>

<?= validation_errors("<p style='color: red';>", '</p>') ?>

<?php 
	if(isset($flash)){
		echo $flash;
	}
?>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Upload File Info</h2>
			<div class="box-icon">
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		
		<div class="box-content" style="padding: 10px;">
			<div class="alert alert-success" style="font-weight: 700;">Yeay, foto kamu berhasil diupload</div>
			<ul>
				<?php foreach ($upload_data as $item => $value):?>
				<li><?php echo $item;?>: <?php echo $value;?></li>
				<?php endforeach; ?>
			</ul>
			<p>
				<?php 
					$edit_url_page = base_url()."store_items/create/".$update_id;
				?>
				<a href="<?= $edit_url_page ?>"><button type="submit" class="btn btn-primary">Kembali ke halaman edit</button></a>
			</p>
		</div>  

	</div><!--/span-->
</div><!--/row-->