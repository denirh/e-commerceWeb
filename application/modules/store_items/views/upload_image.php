<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Upload Image</h2>
			<div class="box-icon">
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		
		<div class="box-content" style="padding: 10px;">
			<?php 
				if(isset($error))
				foreach ($error as $value) {
					echo $value;
				}
			?>

			
			<?php
				$attributes = array('class' => 'form-horizontal'); 
				echo form_open_multipart('store_items/do_upload/'.$update_id, $attributes);
			?>
			  <fieldset>
				<h4 style="margin-top: 20px;">Pilih foto kamu untuk diupload</h4>
				<div class="control-group" style="height: 200px;">
				  <label class="control-label" for="fileInput">File input</label>
				  <div class="controls">
					<input class="input-file uniform_on" id="fileInput" type="file" name="userfile">
				  </div>
				</div>    

				<div class="form-actions">
				  <button type="submit" class="btn btn-primary">Upload</button>
				  <button type="submit" name="submit" value="Cancel" class="btn">Cancel</button>
				</div>
			  </fieldset>
			</form>   


			<?php
			/*

			<?php echo form_open_multipart('store_items/do_upload/'.$update_id);?>

			<input type="file" name="userfile" size="20" />
			<br/><br/>
			<input type="submit" class="btn btn-primary" value="upload" />
			</form>
			*/
			?>
		</div>  

	</div><!--/span-->

</div><!--/row-->