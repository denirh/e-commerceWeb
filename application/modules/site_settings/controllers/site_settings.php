<?php
	class Site_settings extends MX_Controller
{

function __construct() {
	parent::__construct();
}

function _get_item_segments(){
	// return the segments for the store_item pages (produce pages)
	$segments = "musical/instrument/";
	return $segments;
}

function _get_items_segments(){
	// return the segments for the store_item pages (produce pages)
	$segments = "music/instruments/";
	return $segments;
}

function _get_page_not_found_msg(){
	$msg = '<h1>Oh no, sepertinya anda tersesat di bumi !</h1>';
	$msg .= '<p>Silahkan cek ulang alamat anda dan coba lagi !</p>';
	return $msg;
}

}