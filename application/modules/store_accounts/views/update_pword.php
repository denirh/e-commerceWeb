<h1 style="margin-bottom: 20px;"><?= $headline ?></h1>

<?= validation_errors("<p style='color: red';>", '</p>') ?>

<?php 
	if(isset($flash)){
		echo $flash;
	}
?>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Account Password</h2>
			<div class="box-icon">
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<?php 
				$post_to = base_url()."store_accounts/update_pword/".$update_id;
			?>
			<form class="form-horizontal" method="POST" action="<?= $post_to; ?>">
			  <fieldset>	
				
				<div class="control-group">
					<label class="control-label" for="typeahead">Password </label> 
						<div class="controls"> 
							<input type="password" class="span6" id="typeahead" name="pword"> 
						</div> 
				</div>
				<div class="control-group">
					<label class="control-label" for="typeahead">Repeat Password </label> 
						<div class="controls"> 
							<input type="password" class="span6" id="typeahead" name="repeat_pword"> 
						</div> 
				</div>

				<div class="form-actions">
				  <button type="submit" class="btn btn-primary" name="submit" value="Submit">Save changes</button>
				  <button type="submit" class="btn" name="submit" value="Cancel">Cancel</button>
				</div>
			  </fieldset>
			</form>   

		</div>
	</div><!--/span-->

</div><!--/row-->