
<?php
	class Templates extends MX_Controller
{

	function __construct() {
		parent::__construct();
	}

	function test(){
		$data="";             // mendefinisikan data
		$this->admin($data);  // memilih tindakan
	}

	function public_bootstrap($data){
		// mendapatkan data dari module
		if (!isset($data['view_module'])){
			$data['view_module'] = $this->uri->segment(1);
		}
		$this->load->view('public_bootstrap', $data);
	}

	function public_jqm($data){
		// mendapatkan data dari module
		if (!isset($data['view_module'])){
			$data['view_module'] = $this->uri->segment(1);
		}
		$this->load->view('public_jqm', $data);
	}

	function admin($data){
		// mendapatkan data dari module
		if (!isset($data['view_module'])){
			$data['view_module'] = $this->uri->segment(1);
		}
		$this->load->view('admin', $data);
	}

}
