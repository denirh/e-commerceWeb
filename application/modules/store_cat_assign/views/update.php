<h1 style="margin-bottom: 20px;"><?= $headline ?></h1>

<?= validation_errors("<p style='color: red';>", '</p>') ?>

<?php 
	if(isset($flash)){
		echo $flash;
	}
?>
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Update Category</h2>
			<div class="box-icon">
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<?php 
				$form_location = base_url()."store_cat_assign/submit/".$item_id;
			?>
			<form class="form-horizontal" method="POST" action="<?= $form_location; ?>">
			  <fieldset>
				<div class="control-group">
				  <label class="control-label" for="typeahead">New Option </label>
				  <div class="controls">
					<?php 
						$data_id_dd = 'id="categories"';
						echo form_dropdown('cat_id', $options, $cat_id, $data_id_dd); 
					?>
				  </div>
				</div>
				<div class="form-actions">
				  <button type="submit" class="btn btn-primary" name="submit" value="Submit">Save changes</button>
				  <button type="submit" class="btn" name="submit" value="Finished">Finished</button>
				</div>
			  </fieldset>
			</form>   

		</div>
	</div><!--/span-->

</div><!--/row-->

<?php if($num_rows>0){ ?>
<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Category Selected</h2>
			<div class="box-icon">
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered ">
			  <thead>
				  <tr>
				  	  <th>Count</th>
					  <th>Colour</th>
					  <th>Action</th>
				  </tr>
			  </thead>   
			  <tbody>
			  	<?php
			  		$no=1; 
			  		$this->load->module('store_categories');
					foreach($query->result() as $row) {
						$delete_url = base_url()."store_cat_assign/delete/".$row->id;
						$parent_cat_title = $this->store_categories->_get_parent_cat_title($row->cat_id);
						$cat_title = $this->store_categories->_get_cat_title($row->cat_id);
						$full_cat = $parent_cat_title." > ".$cat_title;
				?>
				<tr>
					<td><?php echo $no++ ?></td>				
					<td><?php echo $full_cat; ?></td>
					<td class="center">
						<a class="btn btn-danger" href="<?= $delete_url ?>">
							<i class="halflings-icon white trash"></i>Hapus  
						</a>
					</td>				
				</tr>
				<?php } ?>
			  </tbody>
		  </table>            
		</div>
	</div><!--/span-->

</div><!--/row-->
<?php } ?>
