<div class="cart" style="margin-top: 24px;">
	<div class="content" style="background-color: #ddd; border-radius: 7px; padding: 7px;">
		<table class="table">
			<tr>
				<td colspan="2">Item ID : <?= $item_id ?></td>
			</tr>
			<?php if($num_colours>0) {?>
			<tr>
				<td>Colour:</td>
				<td>
					<?php 
						$data_id_dd = 'class="form-control"';
						echo form_dropdown('status', $colour_options, $submitted_colour, $data_id_dd); 
					?>
				</td>
			</tr>
			<?php } ?>
			<?php if($num_sizes>0) {?>
			<tr>
				<td>Size:</td>
				<td>
					<?php 
						$data_id_dd = 'class="form-control"';
						echo form_dropdown('status', $size_options, $submitted_size, $data_id_dd); 
					?>
				</td>
			</tr>
			<?php } ?>
			<tr>
				<td>Qty :</td>
				<td>
					<div class="col-sm-5" style="padding-left: 0;">
						<input type="text" class="form-control" >
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;">
					<button class="btn btn-primary"><span class="glyphicon glyphicon-shopping-cart"></span> Add To Basket</button>
				</td>
			</tr>
		</table>
	</div>
</div>

